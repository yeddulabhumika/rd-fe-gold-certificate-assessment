describe("GET /books", function () {
  it("should return all books", function (done) {
    request
      .get("/books")
      .expect(200)
      .end(function (err, res) {
        expect(res.body).toEqual(books);
        done();
      });
  });
});

describe("GET /books/:id", function () {
  it("should return a single book by id", function (done) {
    var bookId = books[0].id;
    request
      .get(`/books/${bookId}`)
      .expect(200)
      .end(function (err, res) {
        expect(res.body).toEqual(books[0]);
        done();
      });
  });

  it("should return 404 if book is not found", function (done) {
    var invalidBookId = "invalid-id";
    request.get(`/books/${invalidBookId}`).expect(404).end(done);
  });
});
describe("PUT /books/:id", function () {
  it("should update an existing book", function (done) {
    var bookId = books[0].id;
    var updatedBook = {
      title: "Design Patterns & Design Principles",
      author: "Richard Wick",
      price: 520,
    };
    request.put(`/books/${bookId}`).send(updatedBook).expect(200);
  });

  it("should return 404 if book is not found", function (done) {
    var invalidBookId = "invalid-id";
    var updatedBook = {
      title: "updated_tit",
      author: "updated_auth",
      price: 140,
    };
    request
      .put(`/books/${invalidBookId}`)
      .send(updatedBook)
      .expect(404)
      .end(done);
  });

  it("should return 400 if request body is invalid", function (done) {
    var bookId = books[0].id;
    var invalidBook = {
      title: "updated_tit",
      author: "updated_auth",
      price: 140,
    };
    request.put(`/books/${bookId}`).send(invalidBook).expect(400).end(done);
  });
});
