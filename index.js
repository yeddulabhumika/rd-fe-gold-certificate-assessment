const Joi = require("joi");
const bodyParser = require("body-parser");
const app = require("express")();
const validator = require("express-joi-validation").createValidator({});
const uuid = require("uuid");
app.use(bodyParser.json());

//valdations to fields
const bookSchema = Joi.object({
  title: Joi.string().required(),
  author: Joi.string().required(),
  price: Joi.number().integer().min(100).max(10000).required(),
});

// Test data or mock books array
let books = [
  {
    id: uuid.v4(), //generates id uniquely
    title: "JavaScript Fundamentals",
    author: "chowdham",
    price: 1250,
  },
  {
    id: uuid.v4(),
    title: "JAVA complete edition",
    author: "RK publishers",
    price: 1300,
  },
  {
    id: uuid.v4(),
    title: "FrontEnd Development",
    author: "VD brothers",
    price: 1520,
  },
  {
    id: uuid.v4(),
    title: "FullStack Complete Reference",
    author: "HenryFord",
    price: 1820,
  },
  {
    id: uuid.v4(),
    title: "Design Patterns & Design Principles",
    author: "Richard Wick",
    price: 520,
  },
];

// Get all books with pagination->displays current page and Total number of pages
//example: http://localhost:3000/books
app.get("/books", (req, res) => {
  const page = parseInt(req.query.page) || 1; // Current page number
  const limit = parseInt(req.query.limit) || 8; // Number of books per page

  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;

  const booksSlice = books.slice(startIndex, endIndex);
  const totalPages = Math.ceil(books.length / limit);

  res.json({
    books: booksSlice,
    currentPage: page,
    totalPages: totalPages,
  });
});

// Get book details by particular id
//example: http://localhost:3000/books/74180ad1-8f3a-f85ccd7
app.get("/books/:id", (req, res) => {
  const book = books.find((book) => book.id === req.params.id);
  res.json(book);
});

//add book details by post method
//example: http://localhost:3000/books details of book to be entered in postman.
app.post("/books", (req, res) => {
  const { title, author, price } = req.body;
  const { error, value } = bookSchema.validate({ title, author, price });

  if (error) {
    return res.status(400).json({ message: error.details[0].message });
  }

  const newBook = { id: uuid.v4(), title, author, price };
  books.push(newBook);
  res.status(201).json(newBook);
});

//update existing book by using put method
//example: http://localhost:3000/books/74180ad1-8f3a-f85ccd7 details of book to be entered in postman.
app.put("/books/:id", (req, res) => {
  const book = books.find((book) => book.id === req.params.id);
  if (!book) {
    return res.status(404).json({ message: "Book not found" });
  }

  const { title, author, price } = req.body;
  const { error, value } = bookSchema.validate({ title, author, price });

  if (error) {
    return res.status(400).json({ message: error.details[0].message });
  }
  if (title) {
    book.title = title;
  }
  if (author) {
    book.author = author;
  }
  if (price) {
    book.price = price;
  }
  res.json(book);
});

// Delete a book by its id
//example: http://localhost:3000/books/74180ad1-8f3a-47a0-a0d8-b863cf85ccd7
app.delete("/books/:id", (req, res) => {
  const bookId = req.params.id;
  const bookIndex = books.findIndex((book) => book.id === bookId);

  if (bookIndex === -1) {
    return res.status(404).json({ message: "Book not found" });
  }
  books = books.filter((book) => book.id !== bookId);
  res.sendStatus(204);
});

// Get search and limit the  list of books, sorted by title property and filtered by titleSubstring in the title property.
//example: http://localhost:3000/books/searchandlimit/JAVA/1

app.get("/books/searchandlimit/:titleSubstring/:limit", (req, res) => {
  const { titleSubstring, limit } = req.params;
  const filteredBooks = books
    .filter((book) => book.title.includes(titleSubstring))
    .sort((a, b) => a.title.localeCompare(b.title))
    .slice(0, limit);
  res.json(filteredBooks);
});

//returns all the book details of particular author
app.get("/books/searchbyauthor/:authorSubstring/", (req, res) => {
  const { authorSubstring } = req.params;
  const filteredBooks = books
    .filter((book) => book.author.includes(authorSubstring))
    .sort((a, b) => a.author.localeCompare(b.author));
  res.json(filteredBooks);
});

//returns the books that costs less than the particular price passed as url param
app.get("/books/searchbyprice/:priceLessThanEqual/", (req, res) => {
  const { priceLessThanEqual } = req.params;
  const filteredBooks = books.filter(
    (book) => book.price <= priceLessThanEqual
  );
  res.json(filteredBooks);
});

//limits the number  of books to display
app.get("/books/limit/:limit", (req, res) => {
  const { limit } = req.params;
  const filteredBooks = books.slice(0, limit);
  res.json(filteredBooks);
});

const PORT = 3000;
app.listen(PORT, () =>
  console.log(`Server started on port ${PORT}
click on http://localhost:${PORT}/books`)
);
